package `Machine-Problems`

fun main() {
    val phoneBook: MutableMap <String, Any> = mutableMapOf(
        "Ben" to "000000000001",
        "Ana" to "000000000002",
        "Dan" to "000000000003",
        "Cai" to "000000000004",
        "Gel" to "000000000005",
        "Bry" to "000000000006",
        "Sue" to "000000000007",
        "Fei" to "000000000008",
        "Lei" to "000000000009",
        "Jin" to "000000000010"
    )
    var prompt = "Y"
    while(prompt == "Y") {
        println("Would you like to use the phone directory?")
        println("Press Y to use it. Press N to quit")
        prompt = readLine().toString()
        if (prompt == "Y") {
            println("Enter a name:")
            val nameEntered = readLine()!!
            phoneBook += findName(phoneBook, nameEntered, phoneBook.size)
        }
    }
}

fun findName(phoneBook: MutableMap<String,Any>, nameEntered: String, memory: Int): MutableMap<String, Any> {
    var phoneUpdate = mutableMapOf<String,Any>()

    if (phoneBook.containsKey(nameEntered))        {
        println("The phone number of $nameEntered is ${phoneBook[nameEntered]}")
    }else{
        println("There is no record of $nameEntered")
        println("Would you like to create new record with that name?")
        println("Press Y if yes or N if no")
        val answer = readLine()!!
        if (answer == "Y"){
            if(memory < 30) {
                println("Please enter the number:")
                val newNumber: String? = readLine()
                phoneUpdate = (phoneBook + Pair(nameEntered, newNumber)) as MutableMap<String, Any>
                print("You have successfully added $nameEntered to your phone\n")
            }else {println("MEMORY FULL you already have a record of $memory ")}
        }
    }
    return phoneUpdate

}