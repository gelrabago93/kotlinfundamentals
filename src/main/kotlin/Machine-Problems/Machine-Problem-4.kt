package `Machine-Problems`

fun main(){
    val quiz = arrayOf(
        "Question 1\nWhat is the tiny piece at the end of a shoelace called?\nA. aglet B. tube C. pipe",
        "Question 2\nWhat is the tallest breed of dog in the world?\nA. poodle B. Great Dane C. wolf",
        "Question 3\nWhat is the softest mineral in the world?\nA. talc B. zinc C. magnesium",
        "Question 4\nHow many ribs are in a human body?\n A. 20 B. 22 C. 24",
        "Question 5\nWhat is the world’s biggest island?\nA.Iceland B.Greenland C. Poland",
        "Question 6\nWhat color eyes do most humans have?\nA. black B. brown C. blue",
        "Question 7\nWhich bone are babies born without?\nA. sternum B. ear bones C.knee cap",
        "Question 8\nHow many hearts does an octopus have?\nA. 1 B. 2 C. 3",
        "Question 9\nHow many eyes does a bee have?\nA. 2 B. 3 C. 5",
        "Question 10\nWhat color is Absinthe?\nA. Blue B. Green C. White"
    )
    val answer = arrayOf("A","B","A","C","B","B","C","C","C","B")
    println("Welcome to Quiz! Please choose the correct answer.")
    var i = 0
    var score = 0
    while(i <= quiz.lastIndex){
        println(quiz[i])
        val response:String = readLine()!!
        if (response == "A" || response ==  "B" || response ==  "C"){
            if (response == answer[i]){
                println("Correct!!!")
                i++
                score++
            }else{
                println("Wrong. The correct answer is ${answer[i]}")
                i++
            }
        }else{
            println("Invalid response!!")
        }
    }
    println("Correct answer: $score.\nIncorrect answer: ${10-score}.")
}