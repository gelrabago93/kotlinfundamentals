package `Machine-Problems`

fun main() {
    println("Please input a String:")
    val inputString:String = readLine()!!
    if (inputString.length <= 15) machineProblem1(inputString)
    else println("Invalid String!!!")
}

fun machineProblem1(input: String) {
    val result:Any
    if (input.length % 2 == 0) {
        result = input.reversed()
        println(result)
    }
    else {
        result = input.toCharArray()
        println(result.sorted().joinToString(""))
    }
}
