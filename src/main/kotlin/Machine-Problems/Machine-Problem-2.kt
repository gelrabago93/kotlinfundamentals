package `Machine-Problems`

fun main() {
    val colors = mutableListOf("red", "orange", "yellow", "RED", "pink", "silver", "YeLLoW")
    val colorsInput = mutableListOf<String>()
    colors.forEach() {
        colorsInput += it.lowercase()
    }
    counter(colorsInput)

}

fun counter(inputList: MutableList<String>) {
    val rainbow = listOf("red", "orange", "yellow", "green", "blue", "indigo", "violet")
    var dummy = inputList
    inputList.forEach {
        if (!rainbow.contains(it)) {
            dummy = ((dummy - it + "others").toMutableList())
        }
    }
    println(dummy.groupingBy { it }.eachCount())

}