import kotlin.reflect.typeOf

fun main(args: Array<String>) {
    // functions for ATMs
    // Check balance -> "Your balance is: ____"
    // Deposit -> "You just deposited ____. Your new balance is: ___"
    // Withdrawal -> "Your new balance is: ___"

    // Deposit
    // 1. You cannot deposit more than 5000
    // 2. You cannot deposit an amount not divisible by 100
    // 3. You cannot deposit negative amount

    // Withdraw
    // 1. You cannot withdraw more than half of the current amount
    // 2. You cannot withdraw negative amount

    val account: MutableMap<String, Any> = mutableMapOf(
        "username" to "Brandon",
        "balance" to 0
    )

    println(checkBalance(account))
    deposit(account, 500)
    withdraw(account, 200)
    deposit(account, 100)
    withdraw(account, 50)
}

fun checkBalance(account: MutableMap<String, Any>): String {
    return "Your balance is ${account.getValue("balance")}"
}

fun deposit(account: MutableMap<String, Any>, amount: Int): Unit {
    val newBalance = account.getValue("balance").toString().toInt() + amount
    account["balance"] = newBalance

    println("You just deposited: $amount. Your new balance is $newBalance")
}

fun withdraw(account: MutableMap<String, Any>, amount: Int) {
    val newBalance = account.getValue("balance").toString().toInt() - amount
    account["Balance"] = newBalance

    println("You just deposited: $amount. Your new balance is $newBalance")
}


